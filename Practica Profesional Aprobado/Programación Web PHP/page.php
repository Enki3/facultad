<?php
class pagina_Web
{
public $titulo;
public function __construct($titulo = "Titulo por defecto")
{
   $this->setTitulo($titulo); 
}
private function setTitulo($titulo)
{
	$this->titulo = $titulo;
}
public function getTitulo()
{
	return $this->titulo;
}
protected function cabecera()
{
	echo ("<html><head><title>") ;
	echo $this->titulo;
	echo (" </title></head><body>");
}
private function cuerpo()
{
	echo("Este es el cuerpo de la página Web");
} 

protected function pie ( )
{
	echo ("</bodyx/html>") ;
}
public function mostrar_pagina()
{
	echo $this->cabecera() ;
	echo $this->cuerpo() ;
	echo $this->pie();
}
}

class pagina_Web_formulario extends pagina_Web
{ 
public $action;
private function formulario_inicio($accion,$method="POST")
{
	echo ("<form action=\"$accion\" method=\"$method\">");
}
private function formulario_fin()
{
	echo ("</form>");
}
private function formulario_label($texto,$input){
	echo "<label for=\"$input\">$texto</label>";
}
private function formulario_caja_texto($texto,$nombre,$type="text")
{
	$this->formulario_label($texto,$nombre);
	echo ("<input type=\"$type\" name=\"$nombre\"><br>");
}
private function formulario_boton()
{
	echo ("<input type=\"submit\" name=\"Submit\" value = \"Enviar\">" ) ;
}
public function mostrar_pagina()
{
	$this->cabecera();
	$this->formulario_inicio("index_page.php") ;
	$this->formulario_caja_texto("Documento Nacional de Identidad","dni","number") ;
	$this->formulario_caja_texto("Nombre","nombre") ;	
	$this->formulario_caja_texto("Apellido","apellido");
    $this->formulario_caja_texto("Fecha de Nacimiento","fecha_nac","date");
	$this->formulario_boton();
	$this->formulario_fin();
	$this->pie();
}
}

//$pagina = new pagina_Web(); 
//$pagina->mostrar_pagina() ; 

/*$pagina = new pagina_Web("Aprendiendo PHP"); 
$pagina->mostrar_pagina() ; */

$formulario = new pagina_Web_formulario("Página con formulario");
$formulario->mostrar_pagina( );
